# Copy Workflow Lines

- Copyright: 2019 INGEINT <https://www.ingeint.com>
- Repository: https://bitbucket.org/ingeint/copywfnodes
- License: GPL 2

## Description
This Plugin allow copy WorkFlow nodes of other 

## Contributors
2019 Orlando Curieles orlando.curieles@ingeint.com
## Features/Documentation
- Save time when create a new WorkFlow
## Instructions
- Install the Plugin and will appear the new toolbar bottom in the 
workflow window

## Standard

